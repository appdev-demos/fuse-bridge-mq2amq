## Documentation references

- Installation of AMQ Image Streams \
  https://access.redhat.com/documentation/en-us/red_hat_amq/7.4/html/deploying_amq_broker_on_openshift_container_platform/install-prepare-ocp-broker-ocp#installing-broker-ocp_broker-ocp

# Deploy Red Hat AMQ in OpenShift

Below it is described how to first load the necessary AMQ resources prior to deploy AMQ Brokers.

Two deployment instructions are covered for:
 - non-secure AMQ Broker (basic)
 - tls-enabled AMQ Broker (ssl)

## Prepare the AMQ images

By default OpenShift will not be pre-loaded with AMQ-7 images. The reference document above covers in detail the steps required to load AMQ's images.

The following instructions summarise the key steps required:

1. Login as admin and switch to the 'openshift' namespace:

    ```
    oc login -u system:admin
    oc project openshift
    ```

2. Load the AMQ Image Streams:

    ```
    oc replace --force  -f \
    https://raw.githubusercontent.com/jboss-container-images/jboss-amq-7-broker-openshift-image/74-7.4.0.GA/amq-broker-7-image-streams.yaml
    ```

3. Load/Update the AMQ templates:

    ```
    for template in amq-broker-74-basic.yaml \
    amq-broker-74-ssl.yaml \
    amq-broker-74-custom.yaml \
    amq-broker-74-persistence.yaml \
    amq-broker-74-persistence-ssl.yaml \
    amq-broker-74-persistence-clustered.yaml \
    amq-broker-74-persistence-clustered-ssl.yaml;
    do
    oc replace --force -f \
    https://raw.githubusercontent.com/jboss-container-images/jboss-amq-7-broker-openshift-image/74-7.4.0.GA/templates/${template}
    done
    ```


## Deploy a basic AMQ Broker

The following instructions intend to deploy for demo purposes a Basic AMQ Broker (ephemeral, no SSL).




The If your environment has not been preloaded first 

1. Login to OpenShift as a developer and create a new project (namespace) where AMQ will be deployed (e.g. `brokers`).

    ```
    oc login -u developer -p developer
    oc new-project brokers
    ```

2. Using the CLI run the following `oc` command:

    ```
    oc new-app --template=amq-broker-74-basic \
    -p AMQ_PROTOCOL=openwire,amqp,stomp,mqtt,hornetq \
    -p AMQ_USER=admin \
    -p AMQ_PASSWORD=admin
    ```

## Install from Web Console

As an alternative to the CLI installation, follow the instruction below:

1. Select the option:
    
    - *Add to Project -> Browse Catalog*
    <br>

3. Use the filter `amq` to easily find the following AMQ template:

    - *Red Hat AMQ Broker 7.4 (Ephemeral, no SSL)*
    <br>

   then click on the template. A wizard will open, then click `Next`. 

4. Set the following values and accept the rest of default ones:

    - AMQ Username=admin
    - AMQ Password=admin
    - AMQ Global Max Size=1 gb
    <br>

5. Click `Create`. A new pod will kick off running AMQ.

</br>

## Deploy an SSL AMQ Broker

The following instructions describe how to deploy an SSL AMQ Broker (ephemeral, SSL).

### Create new project

Make sure your SSL Broker is deployed in a different project to avoid conflicts with other basic brokers previously deployed.

Login to OpenShift as a developer and create a new project (namespace) where SSL AMQ will be deployed (e.g. `brokers-ssl`).

    ```
    oc login -u developer -p developer
    oc new-project brokers-ssl
    ```

### Prepare keystores

A pre-requisite prior to deploy the SSL Broker is to have the broker's keystore and truststore preloaded as a Secret. 

1. Prepare the hostname

   The broker certificate's CN needs to match its hostname (route URL) since the client will validate it against the address it is connecting to. For this, ensure you know beforehand the route URL that will expose the SSL Broker, with a pattern similar to:

       {route-name}-{project-name}.apps.{cluster-name}

    For example:

       amqp-brokers-ssl.apps.cluster-london-8983.london-8983.example.opentlc.com

1. Generate a self-signed certificate for the broker keystore:


	   keytool -genkey -alias broker -keyalg RSA -keystore broker.ks -storepass password

    IMPORTANT:
    the first question it will ask is:
    
       What is your first and last name?

    You need here to enter the host (route URL) where the broker will run as above explained, for example:

       amqp-brokers-ssl.apps.cluster-london-8983.london-8983.example.opentlc.com

1. Export the certificate so that it can be shared with clients:

       keytool -export -alias broker -keystore broker.ks -file broker_cert -storepass password

1. Generate a self-signed certificate for the client keystore:

       keytool -genkey -alias client -keyalg RSA -keystore client.ks -storepass password
	
1. Create a client truststore that imports the broker certificate:

       keytool -import -alias broker -keystore client.ts -file broker_cert -storepass password
	
1. Export the client’s certificate from the keystore:

       keytool -export -alias client -keystore client.ks -file client_cert -storepass password
	
1. Import the client’s exported certificate into a broker SERVER truststore:

       keytool -import -alias client -keystore broker.ts -file client_cert -storepass password


1. Create Secret AMQ will use for SSL

       oc create secret generic amq-app-secret --from-file=broker.ks --from-file=broker.ts

	> **Note:** here the broker.ts is not actually used by the broker because it's only performing 1-way TLS)
	
    > **Note:** 'amq-app-secret' is the default secret name the SSL Broker will look for during the deployment.

</br>

### Deployment

Using the CLI run the following `oc` command:


    oc new-app --template=amq-broker-74-ssl \
    -p AMQ_PROTOCOL=amqp \
    -p AMQ_USER=admin \
    -p AMQ_PASSWORD=admin \
    -p AMQ_KEYSTORE_PASSWORD=password \
    -p AMQ_TRUSTSTORE_PASSWORD=password

And finally, to expose the Broker to external clients, create a passthrough Route connecting to the AMQP service.

    oc create route passthrough amqp --service=broker-amq-amqp-ssl

