## Documentation references

- Fuse7 Persistence Manual (IBM-MQ instructions) \
  http://people.apache.org/~ggrzybek/fuse7-persistence-manual.html#ibm-mq
- MQ JMS Tutorial on SpringBoot \
  https://ibmcode-staging.us-east.containers.mybluemix.net/tutorials/mq-jms-application-development-with-spring-boot/

## Index

The following content provides instructions on how to:

 - Deploy IBM-MQ Broker
 - Secure Broker with TLS


# Deploy IBM-MQ in OpenShift

1. Login as a developer to OpenShift and create a new project (namespace) where IBM-MQ will be deployed (e.g. `brokers`).

    ```
    oc login -u developer -p developer
    oc new-project brokers
    ```

2. Using the CLI run the following `oc` command:

    ```
    oc new-app ibmcom/mq \
    -e LICENSE=accept \
    -e MQ_QMGR_NAME=QM1
    ```
    
    > **Note**: IBM-MQ's image needs to run as root, depending on your environment the pod might fail to start with the error message: \
       > *'Error setting admin password: /usr/bin/sudo: exit status 1: sudo: PERM_SUDOERS: setresuid(-1, 1, -1): Operation not permitted'* \
      In such case, with admin rights execute the following admin command to allow the pod to run as root: \
      `oc adm policy add-scc-to-user anyuid -z default` \
      Then try launching the pod again.

3. Create a route to expose MQ's web console:

    ```
    oc create route passthrough --service=mq --port 9443
    ```
    

## Install from Web Console

As an alternative to the CLI installation, follow the instruction below:

1. Select the option:
    
    - *Add to Project -> Deploy Image*
    <br>

3. Select `Image Name` on the presented wizard and populate with:

   - `ibmcom/mq`
    <br>

   then click `Search` or the Enter key. The last action should find the image from DockerHub. 

4. Enter the following Environment name/value pairs:

    - LICENSE=accept
    - MQ_QMGR_NAME=QM1
    <br>

5. Click `Deploy`. A new pod will kick off running MQ.


</br>

## Secure Broker with TLS

The instructions below with modify the deployed MQ broker to enable TLS (1-way authentication)

> **ATTENTION:** \
The following actions will secure the default connectivity port (1414). This means previous non-secure client connections will fail to interact with the Broker. If you want to prevent non-secure clients from failing, simply deploy a new Broker in a new project and secure its connectivity as per the instructions below.

### Reference document:

    https://developer.ibm.com/messaging/learn-mq/mq-tutorials/secure-mq-tls/

1. Create TLS Key & Cert:

	   openssl req -newkey rsa:2048 -nodes -keyout key.key -x509 -days 365 -out key.crt -subj "/C=GB/ST=London/L=London/O=Global Security/OU=IT Department/CN=example.com"

    This should generate the following files:
    - key.crt
    - key.key

    \
    </br>

1. Create Secret in OCP

    Ensure you're working on the project where MQ has been deployed and run the folliwing command:

	   oc create secret tls mqcert --cert ./key.crt --key ./key.key

    This will create a Secret that contains 1 key for each file with its file content as a value


1. Update MQ's deployment configuration to mount the secret

	   oc set volume dc/mq --add -t secret -m /etc/mqm/pki/keys/mykey --name cert --secret-name mqcert

	This will map the secret keys (files) in the container's directory where MQ looks for certificates.
	After doing so, the pod with reboot and MQ will run TLS enabled.


1. To prepare a Java (JKS) truststore that includes MQ's certificate, run the following command:

       keytool -keystore clientkey.jks -storepass password -storetype jks -importcert -file key.crt -alias server-certificate -noprompt

  The above will result producing a JKS truststore to be used by the Java client connecting to MQ.


