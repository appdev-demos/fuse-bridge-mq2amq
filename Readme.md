# How to prepare the Demo

This sample project showcases how Fuse can be used to bridge messages from IBM-MQ to AMQ.

Fuse would use IBM's MQ JMS libraries to consume incoming JMS events from IBM MQ. Then Fuse would convert the incoming JMS messages and send them to an AMQ Broker. Fuse uses the AMQP component (JMS API under the hood) to send the messages to AMQ.


## Dependencies

### IBM-MQ

The project requires an IBM-MQ instance as source. The easiest approach to run it is to predeploy an IBM-MQ container. \
Read the instructions following this __[link](docs/deploy-mq.md)__ on how to do so.


### AMQ-7

The project requires an AMQ Broker as destination. One approach is to download AMQ and run an instance. This example was tested by also pre-deploying an AMQ Broker instance running in a container: \
Read the instructions following this __[link](docs/deploy-amq.md)__ on how to do so.


## Running the example

Once you've pre-deployed an instance of each (IBM-MQ and AMQ), you can simply run the Fuse example in your machine having pre-configured your environment parameters.

If you followed the above instructions and are running both brokers in OpenShift, the current default values would work assuming you _port-forward_ the traffic to the pods.

<br>

### Port-Forward to IBM-MQ

From a new terminal window, and using the `oc` client, ensure you're placed in the namespace where IBM-MQ is running, and execute the following command:

    oc get pods

Locate the pod that corresponds to IBM-MQ and execute the following CLI command from a new terminal to port-forward traffic.

    oc port-forward <pod-id> 1414

where `<pod-id>` corresponds to the pod where IBM-MQ is running.

<br>

### Port-Forward to AMQ-7

From a new terminal window, and using the `oc` client, ensure you're placed in the namespace where AMQ is running, and execute the following command:

    oc get pods

Locate the AMQ pod and execute the following CLI command from a new terminal to port-forward traffic.

    oc port-forward <pod-id> 5672

where `<pod-id>` corresponds to the pod where AMQ is running.


<br>

### Run Fuse Locally

Once you've set the 'port-forward' processes, run Fuse locally by simply executing the following _Maven_ instruction:

    mvn

The above command should launch the application locally and you should see in the terminal how messages start migrating from IBM-MQ to AMQ-7:

    ... INFO  mq-producer   - MQ Producer: Message: 1
    ... INFO  mq-consumer   - mq2amq Bridge: Message: 1
    ... INFO  amqp-consumer - AMQP Consumer: Message: 1


### Manage MQ from its web console

You can open MQ's web console and browse/manage configuration and statistics.

1. Obtain MQ's web console URL with the following `oc` command:

	```
	oc get route mq
	```

2. Open a browser, and hit the web console with the URL obtained above:

		https://{url-mq}

3. Login to the console using the following credentials:

	- Username: admin
	- Password: passw0rd

<br>

### Manage AMQ-7 from its web console

You can open AMQ-7's web console and browse/manage configuration and statistics.

1. Obtain AMQ's web console URL with the following `oc` command:

	```
	oc get route console
	```

2. Open a browser, and hit the web console with the URL obtained above:

		http://{url-amq7}

3. Select the option _'Management Console'_ and login using the following credentials:

	- Username: admin
	- Password: admin


</br>


### Run Fuse Locally against TLS enabled Brokers

This section describes how to run the bridge against secured (TLS enabled) brokers.

The prerequisites are:
 - IBM's MQ Broker has been secured with TLS connectivity.
 - AMQ's Broker has been secured with TLS connectivity.

Instructions on how to secure the Brokers are included in the respective broker installation instructions, check them out.

#### Fuse configuration

The project has been already pre-loaded with certificates and keystores, however you'll probably need to recreate them to fit your environment. Instructions are included in the respective broker installation instructions.

The folder including all related resources is:

	./certificates

Once certificates and secure brokers are ready and available, update the following configuration file:

	./src/main/resources/application.properties

Comment out the following entry, it should look like:

```properties
# AMQP Connectivity
# broker.amqp.uri=amqp://localhost:5672
```

And comment in the AMQPS configuration, which should be:

```properties
# AMQPS Connectivity
broker.amqp.uri=amqps://somehost:443?...
```

Ensure the hostname matches your Route URL connecting to your secure AMQPS service.

Finally also edit the following file:

	./src/main/resources/spring/camel-context.xml

And comment out the property *SSLCipherSuite* which is required to indicate the cipher to use against the broker, it should look like:

```xml
<property name="SSLCipherSuite" value="${ibm.mq.sslCipherSuite}"/>
```

#### Connection Setup

For simplicity, your connectivity to your secure IBM-MQ broker will still be using a proxy (via port-forward). Ensure your proxy is open before starting fuse.

However, this time the connectivity to AMQ SSL will use the route instead of the proxy.

#### Run Fuse

To secure the connection to IBM's MQ, we need to pass a set of system properties to the JVM:

 - javax.net.ssl.trustStoreType
 - javax.net.ssl.trustStore
 - javax.net.ssl.trustStorePassword
 - com.ibm.mq.cfg.useIBMCipherMappings=false

We can pass the above as Maven parameters when starting Fuse.

Launch Fuse with the following command, ensuring the values are correct:

	mvn \
	-Djavax.net.ssl.trustStoreType=jks \
	-Djavax.net.ssl.trustStore=./certificates/mq-client/clientkey.jks \
	-Djavax.net.ssl.trustStorePassword=password \
	-Dcom.ibm.mq.cfg.useIBMCipherMappings=false

